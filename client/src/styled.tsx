import styled, { css, createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body{margin: 0; overflow-x:hidden;}
  div ,input,button {box-sizing:border-box;}
  img [src=""]{display: none;}
`;

export const betweenCenter = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const TextInput = styled.input.attrs({ type: "text" })`
  width: ${(props) => props.width || "10rem"};
  height: ${(props) => props.height || "2.5rem"};
  border: none;
  padding: 5px;
  border-radius: ${(props) => props.radius || "0.2rem"};
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
  border-bottom: 2px solid #fff;
  background-color: ${(props: { bgc?: string; radius?: string }) =>
    props.bgc || "rgb(255 250 250 / 69%)"};
  color: #fff;
`;

export enum TStatus {
  PRIMARY = "primary",
  DEFAULT = "default",
  ERROR = "error",
}

const Status: { [prop: string]: string } = {
  [TStatus.PRIMARY]: "#51f",
  [TStatus.DEFAULT]: "#fff",
  [TStatus.ERROR]: "#f00",
};

export const Button = styled.button`
  border: none;
  background-color: ${(props: {
    status?: TStatus;
    height?: string;
    width?: string;
  }) => Status[props.status as TStatus] || Status[TStatus.DEFAULT]};
  border-radius: 3px;
  height: ${(props) => props.height || "2rem"};
  width: ${(props) => props.width || "4rem"};
  color: ${(props) => props.color || "#FFF"};
  font-weight: bold;
  box-shadow: 0px 0px 3px #ccc;

  &:active {
    background-color: #cccccc92;
    transform: translateY(1px);
  }
`;

export const textEllipsis = css`
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`;
