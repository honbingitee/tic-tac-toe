import { FC, ReactElement, useEffect, useState } from "react";
import Grid from "./Grid";
import Char from "./Char";
import OnlinePlayers from "./OnlinePlayers";
import Mask, { showMask, hideMask } from "../ui/Mask";
import FightToast from "./FightToast";
import { IFightData, IOpponent } from "./types";
import styled from "styled-components";

interface IProps {}

const TicTacToe: FC<IProps> = (): ReactElement => {
  const [fightData, setFightData] = useState<IFightData>({});
  const [opponent, setOpponent] = useState<IOpponent>({});

  useEffect(() => {
    //蒙版自动化 np 666 我滴宝贝～ 有数据显示没有消失
    if (Object.keys(fightData).length) {
      showMask();
    } else hideMask();
  }, [fightData]);

  return (
    <Container>
      <OnlinePlayers setOpponent={setOpponent} setFightData={setFightData} />
      <Char />
      <Grid opponent={opponent} />
      <Mask />
      <FightToast
        setOpponent={setOpponent}
        setFightData={setFightData}
        fightData={fightData}
      />
    </Container>
  );
};

export default TicTacToe;

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  position: relative;
  height: calc(100vh - 4rem);
`;
