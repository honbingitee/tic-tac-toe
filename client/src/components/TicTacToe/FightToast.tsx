import { FC, ReactElement, useCallback, useMemo } from "react";
import { useSocketContext } from "../../api/context/SocketContext";
import { EMIT } from "../../api/types";
import { TStatus, Button } from "../../styled";
import styled from "styled-components";
import { FightData, IFightData, IOpponent } from "./types";

interface IProps {
  fightData: IFightData;
  setFightData: React.Dispatch<React.SetStateAction<IFightData>>;
  setOpponent: React.Dispatch<React.SetStateAction<IOpponent>>;
}

const FightToast: FC<IProps> = ({
  fightData,
  setFightData,
  setOpponent,
}): ReactElement => {
  const [{ socket }] = useSocketContext();
  const fightArr = useMemo(() => Object.entries(fightData), [fightData]);

  //拒绝某一个对战邀约
  const handleRejectFight = useCallback(
    (fightData: FightData) => () => {
      console.log("拒绝" + fightData.nickname + " 的对战邀约");
      // 给该玩家发送被拒绝通知
      socket.emit(EMIT.REJECT_FIGHT, fightData.socketId);
      //删除这一条对战邀约
      setFightData((prev: any) => {
        const data = { ...prev };
        delete data[fightData.nickname];
        return data;
      });
    },
    [setFightData, socket]
  );

  //同意某一个对战邀约 其他的视为拒绝
  const handleAcceptFight = useCallback(
    ({ nickname, socketId }: FightData) =>
      () => {
        console.log("同意" + nickname + "的对战邀约");
        //发送接受挑战事件
        socket.emit(EMIT.ACCEPT_FIGHT, socketId);
        //存储 对手的信息
        setOpponent(() => ({ nickname, socketId }));
        //清空邀请列表 开始游戏
        setFightData((prev) => {
          const data = { ...prev };
          delete data[nickname];
          // 给其他玩家发送被拒绝通知
          Object.values(data).forEach((fightData) => {
            console.log("给该" + fightData.nickname + "发送被拒绝通知");
            socket.emit(EMIT.REJECT_FIGHT, fightData.socketId);
            delete data[fightData.nickname];
          });
          return data;
        });
        console.log("开始🎮");
      },
    [socket, setOpponent, setFightData]
  );

  return (
    <Container show={!!fightArr.length}>
      {fightArr.map(([nickname, data]: [string, FightData]) => (
        <FightToastStyled key={nickname}>
          <section>
            <strong>{nickname}发来对战邀请</strong>
            <br />
            {data.message}
          </section>
          <div>
            <Button onClick={handleRejectFight(data)} status={TStatus.ERROR}>
              拒绝
            </Button>
            <Button onClick={handleAcceptFight(data)} status={TStatus.PRIMARY}>
              接受
            </Button>
          </div>
        </FightToastStyled>
      ))}
    </Container>
  );
};

export default FightToast;

const Container = styled.div`
  width: 20rem;
  height: 36rem;
  display: flex;
  flex-direction: column;
  overflow-y: hidden;
  position: absolute;
  z-index: 1;
  visibility: ${(props: { show: boolean }) =>
    props.show ? "visible" : "hidden"};
`;

const FightToastStyled = styled.div`
  width: 18rem;
  height: 7rem;
  padding: 1rem;
  border-radius: 3px;
  background: #faf7f7;
  margin: 1rem;
  box-shadow: 8px 8px 38px #e0dddd, -8px -8px 38px #eeebeb;

  section {
    height: 3rem;
    color: #000;
  }

  & > div {
    display: flex;
    align-items: center;
    justify-content: flex-end;

    & > button {
      margin: 0 0.5rem;
    }
  }
`;
