export interface FightData {
  nickname: string;
  message: string;
  socketId: string;
}

export interface IFightData {
  [nickname: string]: FightData;
}

export interface IWaitFight {
  [nickname: string]: string;
}

export interface IOpponent {
  nickname?: string;
  socketId?: string;
}
