import { FC, ReactElement, useRef, useCallback } from "react";
import styled from "styled-components";
import { useSocketContext } from "../../api/context/SocketContext";
import { OPPONENT, NICKNAME } from "../../api/types";
import { Button, TextInput, TStatus } from "../../styled";

interface IProps {}

const Char: FC<IProps> = (): ReactElement => {
  return (
    <Container>
      <MessageBox></MessageBox>
      <InputBox />
    </Container>
  );
};

export default Char;

const MessageBox = () => {
  return <MessageBoxStyled></MessageBoxStyled>;
};

const InputBox = () => {
  const [{ socket }] = useSocketContext();
  const input = useRef<HTMLInputElement>();

  const handleSendMessage = useCallback(() => {
    const self = localStorage.getItem(NICKNAME) || "";
    const opponent: { nickname: string; socketId: string } = JSON.parse(
      localStorage.getItem(OPPONENT) || "{}"
    );
    if (opponent) {
    }
    console.log(self, input.current!.value, opponent.nickname);
    // socket.to
  }, []);

  return (
    <InputBoxStyled>
      <TextInput
        //@ts-ignore
        ref={input}
        radius="0px"
        style={{ border: "none" }}
        bgc="#48485c3d"
        width="100%"
      />
      <Button
        onClick={handleSendMessage}
        style={{ marginLeft: "1rem" }}
        status={TStatus.PRIMARY}
      >
        SEND
      </Button>
    </InputBoxStyled>
  );
};

const Container = styled.div`
  width: 25rem;
  height: 15rem;
  position: absolute;
  bottom: 0;
  left: 0;
  border-top-right-radius: 5px;
  background: linear-gradient(145deg, #e1e1e1ce, #ffffff);
  box-shadow: 9px 9px 12px #b2b2b2ce, -9px -9px 12px #ffffff;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 1rem;
`;

const MessageBoxStyled = styled.div`
  flex: 1;
  max-height: 10rem;
  background-color: #c9bcbc37;
  overflow-x: hidden;
  overflow-y: scroll;
`;

const InputBoxStyled = styled.div`
  display: flex;
  align-items: center;
`;
