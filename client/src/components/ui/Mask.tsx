import { FC, ReactElement, createRef } from "react";
import styled from "styled-components";

export const MaskRef: React.RefObject<any> = createRef();

export const showMask = () => {
  MaskRef.current.style["visibility"] = "visible";
  MaskRef.current.style["opacity"] = 1;
};

export const hideMask = () => {
  setTimeout(() => {
    MaskRef.current.style["visibility"] = "hidden";
  }, 500);
  MaskRef.current.style["opacity"] = 0;
};

const Mask: FC = (): ReactElement => <Container ref={MaskRef} />;

export default Mask;

const Container = styled.div`
  width: 100%;
  height: 100%;
  background-color: #0000004b;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 0;
  visibility: hidden;
  opacity: 0;
  transition: opacity 0.5s ease;
`;
