import { FC, ReactElement } from "react";
import { useSocketContext } from "../../api/context/SocketContext";
import styled from "styled-components";
import { betweenCenter } from "../../styled";
import NicknameInput from "./NicknameInput";

const Header: FC = (): ReactElement => {
  const [socketState] = useSocketContext();

  return (
    <Nav>
      连接状态 {socketState.status ? "👏" : "🤔"}
      <NicknameInput />
    </Nav>
  );
};

export default Header;

const Nav = styled.div`
  width: 100%;
  height: 4rem;
  background-color: rgba(104, 4, 235, 0.856);
  backdrop-filter: blur(10px);
  box-shadow: rgb(238 238 238) -1px 0px 3px 1px;
  ${betweenCenter};
  color: white;
  padding: 0.5rem;
  box-shadow: -5px 0 12px #333;
`;
