import { FC, ReactElement, useState, useCallback, Fragment } from "react";
import styled from "styled-components";
import { NICKNAME } from "../../api/types";
import { betweenCenter, TextInput, Button, textEllipsis } from "../../styled";

interface IProps {}

const NicknameInput: FC<IProps> = (): ReactElement => {
  const [state, setState] = useState(!!localStorage.getItem(NICKNAME));
  const [nickname, setNickname] = useState(
    localStorage.getItem(NICKNAME) || ""
  );

  const handleOnChange = useCallback(
    (input: React.ChangeEvent<HTMLInputElement>) => {
      setNickname(input.currentTarget.value);
    },
    []
  );

  const handleSetNickname = useCallback(() => {
    localStorage.setItem(NICKNAME, nickname);
    setState(true);
  }, [nickname]);

  const handleEditName = useCallback(() => {
    setState(false);
    localStorage.setItem(NICKNAME, "");
  }, []);

  return (
    <Wrapper>
      {state ? (
        <EditWrap>
          <strong>welcome {nickname}</strong>
          <EditIcon handleEditName={handleEditName} />
        </EditWrap>
      ) : (
        <Fragment>
          <TextInput
            value={nickname}
            onChange={handleOnChange}
            maxLength={10}
            placeholder="昵称"
            style={{ marginRight: "1rem" }}
          />
          <Button onClick={handleSetNickname} color="#000">
            确定
          </Button>
        </Fragment>
      )}
    </Wrapper>
  );
};

const EditIcon = ({ handleEditName }: { handleEditName: () => void }) => (
  <svg
    onClick={handleEditName}
    viewBox="0 0 1024 1024"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    p-id="2397"
    width="16"
    height="16"
  >
    <path
      d="M257.7 752c2 0 4-0.2 6-0.5L431.9 722c2-0.4 3.9-1.3 5.3-2.8l423.9-423.9c3.9-3.9 3.9-10.2 0-14.1L694.9 114.9c-1.9-1.9-4.4-2.9-7.1-2.9s-5.2 1-7.1 2.9L256.8 538.8c-1.5 1.5-2.4 3.3-2.8 5.3l-29.5 168.2c-1.9 11.1 1.5 21.9 9.4 29.8 6.6 6.4 14.9 9.9 23.8 9.9z m67.4-174.4L687.8 215l73.3 73.3-362.7 362.6-88.9 15.7 15.6-89zM880 836H144c-17.7 0-32 14.3-32 32v36c0 4.4 3.6 8 8 8h784c4.4 0 8-3.6 8-8v-36c0-17.7-14.3-32-32-32z"
      p-id="2398"
      fill="#ffffff"
    ></path>
  </svg>
);

export default NicknameInput;

const Wrapper = styled.div`
  ${betweenCenter};
`;

const EditWrap = styled.div`
  width: 9rem;
  height: 1rem;
  position: relative;
  ${betweenCenter};
  & > strong {
    transition: all 0.8s ease;
    user-select: none;
    position: absolute;
    right: 0;
    width: 10rem;
    text-align: right;
    letter-spacing: 1px;
    ${textEllipsis};
  }
  & > svg {
    background-color: #dacfcf62;
    border-radius: 2px;
    padding: 1px;
    position: absolute;
    right: 0;
    cursor: pointer;
    transition: all 0.5s ease;
    opacity: 0;
    transform: translateX(1rem);
  }
  &:hover {
    & > strong {
      transform: translateX(-1.5rem);
      letter-spacing: 0px;
    }
    & > svg {
      opacity: 1;
      transform: translateX(0rem);
    }
  }
`;
