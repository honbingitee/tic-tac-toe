import { Socket } from "socket.io-client";
import { DefaultEventsMap } from "socket.io-client/build/typed-events";

export type SOCKET = Socket<DefaultEventsMap, DefaultEventsMap>;

export enum EVENT {
  SEND_MESSAGE = "SEND_MESSAGE",
  CONNECT = "connect",
  ERROR = "connect_error",
  BROADCAST_NEW_GAMER = "BROADCAST_NEW_GAMER",
  WELCOME = "WELCOME",
  BROADCAST_LEAVE_THE_GAME = "BROADCAST_LEAVE_THE_GAME",
  FIGHT = "FIGHT", //接收 对战邀请
  REJECTED_FIGHT = "REJECTED_FIGHT", //对战邀请 被拒绝
  ACCEPTED_FIGHT = "ACCEPTED_FIGHT", //对战邀请 被接受
  SHOULD_GO = "SHOULD_GO", // 当前该谁走了
  PLAYED_CHESS = "PLAYED_CHESS", //对方 落子完毕
  WINED = "WINED", //对方胜利了
}

export enum EMIT {
  ENTER = "ENTER",
  REJECT_FIGHT = "REJECT_FIGHT", //主动拒绝 对战邀请
  ACCEPT_FIGHT = "ACCEPT_FIGHT", //主动接受 对战邀请
  FIGHT_REQUEST = "FIGHT_REQUEST", //主动发送 对战邀请
  PLAY_CHESS = "PLAY_CHESS", //下棋
  WIN = "WIN", //获得了胜利
}

export const NICKNAME = "NICKNAME"; //自己的nickname
export const ALL_GAMERS = "ALL_GAMERS"; //所有连接中的用户
export const OPPONENT = "OPPONENT"; // 对手的信息nickname & socket.id
