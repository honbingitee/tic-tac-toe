import { SOCKET, EMIT } from "./types";
import io from "socket.io-client";

export const createConnect = () => {
  const socket = io("http://localhost:3000", { reconnection: false });

  //连接成功 返回socket实例全局唯一
  socket.on("connect", () => {
    console.log("connect success👏");
    return socket;
  });

  //报错时走这个方法
  socket.on("connect_error", (error: any) => {
    console.error("🥶connect_error:", error);
  });
};

export const enterGame = (socket: SOCKET, name: string) => {
  socket.emit(EMIT.ENTER, name);
};
