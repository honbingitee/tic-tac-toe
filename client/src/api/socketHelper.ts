import { SOCKET, EMIT } from "./types";
import io from "socket.io-client";

export const createConnect = (): SOCKET => {
  const socket = io("http://localhost:3000", { reconnection: false });
  return socket;
};

class SocketHelp {
  socket: SOCKET | undefined;

  connect = () => {
    this.socket = io("http://localhost:3000", { reconnection: false });

    this.socket.on("connect", () => {
      console.log("connect success👏");
      return this.socket;
    });

    //报错时走这个方法
    this.socket.on("connect_error", (error: any) => {
      console.error("🥶connect_error:", error);
    });
  };

  enterGame = (name: string) => {
    this.socket && this.socket.emit(EMIT.ENTER, name);
  };
}

export default new SocketHelp();
