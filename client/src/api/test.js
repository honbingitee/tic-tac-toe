const a = { a: "a", b: "b" };

Object.values(a).forEach((val) => {
  delete a[val];
});

console.log(a);
