import { useState, createContext, useContext } from "react";

export type TSocketContext = [
  {
    status: boolean;
    socket: {};
  },
  React.Dispatch<
    React.SetStateAction<{
      status: boolean;
      socket: {};
    }>
  >
];

const defaultValue: any | TSocketContext = {
  status: false,
  socket: {},
};

const Context: any = createContext(defaultValue);

const SocketContext = ({ children }: any) => {
  const socketState = useState(defaultValue);

  return <Context.Provider value={socketState}>{children}</Context.Provider>;
};

export default SocketContext;
export const useSocketContext: any | TSocketContext = () => useContext(Context);
