const zeroVerify = (index: number, checkerBoard: unknown[], value: string) => {
  return (
    (checkerBoard[index + 2] === checkerBoard[index + 1] &&
      value === checkerBoard[index + 2] &&
      checkerBoard[index + 2] !== undefined) ||
    (checkerBoard[index + 6] === checkerBoard[index + 3] &&
      value === checkerBoard[index + 6] &&
      checkerBoard[index + 6] !== undefined) ||
    (checkerBoard[index + 8] === checkerBoard[index + 4] &&
      value === checkerBoard[index + 8] &&
      checkerBoard[index + 8] !== undefined)
  );
};

const oneVerify = (index: number, checkerBoard: unknown[], value: string) => {
  return (
    (checkerBoard[index + 1] === checkerBoard[index - 1] &&
      value === checkerBoard[index + 1] &&
      checkerBoard[index + 1] !== undefined) ||
    (checkerBoard[index + 6] === checkerBoard[index + 3] &&
      value === checkerBoard[index + 6] &&
      checkerBoard[index + 6] !== undefined)
  );
};

const twoVerify = (index: number, checkerBoard: unknown[], value: string) => {
  return (
    (checkerBoard[index - 2] === checkerBoard[index - 1] &&
      value === checkerBoard[index - 2] &&
      checkerBoard[index - 2] !== undefined) ||
    (checkerBoard[index + 6] === checkerBoard[index + 3] &&
      value === checkerBoard[index + 6] &&
      checkerBoard[index + 6] !== undefined) ||
    (checkerBoard[index + 4] === checkerBoard[index + 2] &&
      value === checkerBoard[index + 4] &&
      checkerBoard[index + 4] !== undefined)
  );
};

const threeVerify = (index: number, checkerBoard: unknown[], value: string) => {
  return (
    (checkerBoard[index + 3] === checkerBoard[index - 3] &&
      value === checkerBoard[index + 3] &&
      checkerBoard[index + 3] !== undefined) ||
    (checkerBoard[index + 2] === checkerBoard[index + 1] &&
      value === checkerBoard[index + 2] &&
      checkerBoard[index + 2] !== undefined)
  );
};

const fourVerify = (index: number, checkerBoard: unknown[], value: string) => {
  return (
    (checkerBoard[index + 1] === checkerBoard[index - 1] &&
      value === checkerBoard[index + 1] &&
      checkerBoard[index + 1] !== undefined) ||
    (checkerBoard[index + 3] === checkerBoard[index - 3] &&
      value === checkerBoard[index + 3] &&
      checkerBoard[index + 3] !== undefined) ||
    (checkerBoard[index + 4] === checkerBoard[index - 4] &&
      value === checkerBoard[index + 4] &&
      checkerBoard[index + 4] !== undefined) ||
    (checkerBoard[index + 2] === checkerBoard[index - 2] &&
      value === checkerBoard[index + 2] &&
      checkerBoard[index + 2] !== undefined)
  );
};

const fiveVerify = (index: number, checkerBoard: unknown[], value: string) => {
  return (
    (checkerBoard[index + 3] === checkerBoard[index - 3] &&
      value === checkerBoard[index + 3] &&
      checkerBoard[index + 3] !== undefined) ||
    (checkerBoard[index - 2] === checkerBoard[index - 1] &&
      value === checkerBoard[index - 2] &&
      checkerBoard[index - 2] !== undefined)
  );
};

const sixVerify = (index: number, checkerBoard: unknown[], value: string) => {
  return (
    (checkerBoard[index - 6] === checkerBoard[index - 3] &&
      value === checkerBoard[index - 6] &&
      checkerBoard[index - 6] !== undefined) ||
    (checkerBoard[index + 2] === checkerBoard[index + 1] &&
      value === checkerBoard[index + 2] &&
      checkerBoard[index + 2] !== undefined) ||
    (checkerBoard[index - 4] === checkerBoard[index - 2] &&
      value === checkerBoard[index - 4] &&
      checkerBoard[index - 4] !== undefined)
  );
};

const sevenVerify = (index: number, checkerBoard: unknown[], value: string) => {
  return (
    (checkerBoard[index - 6] === checkerBoard[index - 3] &&
      value === checkerBoard[index - 6] &&
      checkerBoard[index - 6] !== undefined) ||
    (checkerBoard[index - 1] === checkerBoard[index + 1] &&
      value === checkerBoard[index - 1] &&
      checkerBoard[index - 1] !== undefined)
  );
};

const eightVerify = (index: number, checkerBoard: unknown[], value: string) => {
  return (
    (checkerBoard[index - 6] === checkerBoard[index - 3] &&
      value === checkerBoard[index - 6] &&
      checkerBoard[index - 6] !== undefined) ||
    (checkerBoard[index - 1] === checkerBoard[index - 2] &&
      value === checkerBoard[index - 1] &&
      checkerBoard[index - 1] !== undefined) ||
    (checkerBoard[index - 8] === checkerBoard[index - 4] &&
      value === checkerBoard[index - 8] &&
      checkerBoard[index - 8] !== undefined)
  );
};

const verify = (index: number, checkerBoard: unknown[], value: string) => {
  //获胜方案共有八种
  switch (index) {
    case 0:
      return zeroVerify(index, checkerBoard, value);
    case 1:
      return oneVerify(index, checkerBoard, value);
    case 2:
      return twoVerify(index, checkerBoard, value);
    case 3:
      return threeVerify(index, checkerBoard, value);
    case 4:
      return fourVerify(index, checkerBoard, value);
    case 5:
      return fiveVerify(index, checkerBoard, value);
    case 6:
      return sixVerify(index, checkerBoard, value);
    case 7:
      return sevenVerify(index, checkerBoard, value);
    case 8:
      return eightVerify(index, checkerBoard, value);
    default:
      return false;
  }
};

export default verify;
