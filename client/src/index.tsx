import ReactDOM from "react-dom";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import SocketContext from "./api/context/SocketContext";
import { ThemeProvider } from "styled-components";
import { GlobalStyle } from "./styled";

ReactDOM.render(
  <ThemeProvider theme={{}}>
    <GlobalStyle />
    <SocketContext>
      <App />
    </SocketContext>
  </ThemeProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
