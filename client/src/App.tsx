import { FC, ReactElement } from "react";
import Header from "./components/Header";
import TicTacToe from "./components/TicTacToe";

interface IProps {}

const App: FC<IProps> = (): ReactElement => {
  return (
    <div>
      <Header />
      <TicTacToe />
    </div>
  );
};

export default App;
